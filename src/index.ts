import Vue from "vue";
import App from "./components/App.vue";
import BootstrapVue from "bootstrap-vue";
import "./scss/main.scss";
import store from "./store/index";

Vue.use(BootstrapVue);

const el = document.createElement("div");
document.body.appendChild(el);

new Vue({store, render: h => h(App)}).$mount(el);
