const A = "A";
const B = "B";
const C = "C";
const D = "D";
const E = "E";
const F = "F";
const G = "G";
const H = "H";
const I = "I";
const J = "J";
const K = "K";
const L = "L";
const M = "M";
const N = "N";
const Ñ = "Ñ";
const O = "O";
const P = "P";
const Q = "Q";
const R = "R";
const S = "S";
const T = "T";
const U = "U";
const V = "V";
const W = "W";
const X = "X";
const Y = "Y";
const Z = "Z";
const _ = "";
export { A, B, C, D, E, F, G, H, I, J, K, L, M, N, Ñ, O, P, Q, R, S, T, U, V, W, X, Y, Z, _ };
